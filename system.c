/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#endif

#include <stdint.h>        /* For uint8_t definition */
#include <stdbool.h>       /* For true/false definition */

#include "system.h"

// EEPROM initial settings
// RO holdings
__EEPROM_DATA  (0x07,0xdb,              // 0 // work storage
                0x00,0x00,		//
                0xff,0xff,
                0xff,0xff);             // 7
__EEPROM_DATA  (0xff,0xff,              // 8
                0xff,0xff,
                0xff,0xff,
                0xff,0xff);             // 0xf
__EEPROM_DATA  (0xff,0xff,              // 0x10
                0xff,0xff,
                0xff,0xff,
                0xff,0xff);             // 0x17
__EEPROM_DATA  (0xff,0xff,
                0xff,0xff,
                0xff,0xff,
                0xff,0xff);             // 0x1f
// RW holdings
__EEPROM_DATA  (0x01,0x00,              // 0x20 // work mb addr
                0x00,0xe1,		//      // work baud
                0x00,0x00,		//
                0xff,0xff);
__EEPROM_DATA  (0xff,0xff,              // 0x28
                0xe8,0x03,		//
                0xff,0xff,
                0xff,0xff);
__EEPROM_DATA  (0xff,0xff,              // 0x30
                0xff,0xff,
                0xff,0xff,
                0xff,0xff);
__EEPROM_DATA  (0xff,0xff,              // 0x38
                0xff,0xff,
                0xff,0xff,
                0xff,0xff);
__EEPROM_DATA  (0xff,0xff,              // 0x40
                0xff,0xff,
                0xff,0xff,
                0xff,0xff);
__EEPROM_DATA  (0xff,0xff,              // 0x48
                0xff,0xff,
                0xff,0xff,
                0xff,0xff);
__EEPROM_DATA  (0xff,0xff,              // 0x50
                0xff,0xff,
                0xff,0xff,
                0xff,0xff);
__EEPROM_DATA  (0xff,0xff,              // 0x58
                0xff,0xff,
                0xff,0xff,
                0xff,0xff);
__EEPROM_DATA  (0x00,0x00,		//T0    0x60
                0x00,0x00,
                0x00,0x00,		//C1
                0x80,0x3f);
__EEPROM_DATA  (0x00,0x00,		//C2    0x68
                0x00,0x00,
                0x00,0x00,		//C3
                0x00,0x00);
__EEPROM_DATA  (0x00,0x00,		//F0    0x70
                0x00,0x00,
                0xff,0xff,
                0xff,0xff);
__EEPROM_DATA  (0xff,0xff,              // 0x78
                0xff,0xff,
                0xff,0xff,
                0xff,0xff);
__EEPROM_DATA  (0x00,0x01,		//Coils 0x80
                0x00,0x03,		//
                0x00,0x27,
                0x00,0x00);
__EEPROM_DATA  (0x03,0xff,              // 0x88
                0xff,0xff,
                0xff,0xff,
                0xff,0xff);
// main storage
__EEPROM_DATA  (0x07,0xdb,		// 0x90 ID
                0x00,0x00,		//      SERIAL
                0xff,0xff,
                0xff,0xff);
__EEPROM_DATA  (0xff,0xff,              // 0x98
                0xff,0xff,
                0xff,0xff,
                0xff,0xff);
__EEPROM_DATA  (0xff,0xff,              // 0xa0
                0xff,0xff,
                0xff,0xff,
                0xff,0xff);
__EEPROM_DATA  (0xff,0xff,              // 0xa8
                0xff,0xff,
                0xff,0xff,
                0xff,0xff);
__EEPROM_DATA  (0x01,0x00,		//0xb0  mbaddr
                0x00,0xe1,		//      baud
                0x00,0x00,		//
                0xff,0xff);


/* Refer to the device datasheet for information about available
oscillator configurations and to compiler documentation for macro details. */
inline void ConfigureOscillator(void)
{
    OSCCON=0x00;				//HS PLL Off
#if USE_PLL
    OSCCONbits.SPLLEN = true;
#endif
    CLKRCON=0x00;

#ifndef __DEBUG
    BORCON=0x00;		
    WDTCONbits.WDTPS = 0b1000;                  //WDT On, 256ms
#endif
}

#if 0
uint8_t EEPROM_read_wraper(uint8_t addr) {
    return EEPROM_READ(addr);
}

void EEPROM_write_wraper(uint8_t addr, uint8_t value) {
    EEPROM_WRITE(addr, value);
}
#endif