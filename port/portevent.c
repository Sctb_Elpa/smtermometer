/*
 * FreeModbus Libary: ATMega168 Port
 * Copyright (C) 2006 Christian Walter <wolti@sil.at>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: $Id: portevent.c,v 1.2 2006/05/14 21:55:01 wolti Exp $
 */

/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbport.h"

/* ----------------------- Variables ----------------------------------------*/
static eMBEventType eQueuedEvent;
static BOOL     xEventInQueue;

/* ----------------------- Start implementation -----------------------------*/
BOOL
xMBPortEventInit( void )
{
    xEventInQueue = FALSE;
    return TRUE;
}

BOOL
xMBPortEventPost( eMBEventType eEvent )
{
    xEventInQueue = TRUE;
    eQueuedEvent = eEvent;
    return TRUE;
}

BOOL
xMBPortEventGet( eMBEventType * eEvent )
{
    if( xEventInQueue )
    {
        *eEvent = eQueuedEvent;
        xEventInQueue = FALSE;
        return TRUE;
    }
    return FALSE;
}

void SoftProcessMBEvents() {
    if (RCSTAbits.CREN) {
        if (RCSTAbits.OERR) {
            RCSTAbits.CREN = FALSE;
            RCSTAbits.CREN = TRUE;
        } else if (RCIF) {
            RCIF = FALSE;
            if (RCSTAbits.FERR) {
                !RCREG;
            } else {
                pxMBFrameCBByteReceived();
            }
        }
    }
    if (isTransmitting() && TXIF && TXSTAbits.TRMT) {
        pxMBFrameCBTransmitterEmpty();
    }
#if !MB_TIMER_ISR
    if (T2CONbits.TMR2ON && TMR2IF) {
        TMR2IF = FALSE;
        MB_Timer_ISR();
    }
#endif
}