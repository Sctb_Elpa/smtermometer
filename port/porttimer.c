/*
 * FreeModbus Libary: ATMega168 Port
 * Copyright (C) 2006 Christian Walter <wolti@sil.at>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: $Id: porttimer.c,v 1.4 2006/09/03 11:53:10 wolti Exp $
 */

/* ----------------------- Platform includes --------------------------------*/
#include "port.h"
#include "system.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbport.h"
#include "gloabal_vars.h"

/* ----------------------- Defines ------------------------------------------*/
#define TIMER_CLOCKS_PER_US   (FCY/1000000ul/4)

/* ----------------------- Static variables ---------------------------------*/
static USHORT wTimerVal;
static union U16_2U8 work;

/* ----------------------- Start implementation -----------------------------*/
BOOL
xMBPortTimersInit( USHORT usTim1Timerout50us ) {
    #define TIMER_PRESCALER_AND_POSTSCALER_VALUE  1

    T2CON = 0x00;	//no prescaler no postscaler
#if SYS_FREQ == 10000000ul
    wTimerVal = 125 * usTim1Timerout50us;
#else
    wTimerVal =(TIMER_CLOCKS_PER_US * 50
            * usTim1Timerout50us) /* /TIMER_PRESCALER_AND_POSTSCALER_VALUE */;
#endif
    PR2 = 0xff;

    return TRUE;
}

void
vMBPortTimersEnable( void ) {
    TMR2 = 0;
    work.U16 = wTimerVal;

    TMR2IF = FALSE;
#if MB_TIMER_ISR
    TMR2IE = TRUE;
#endif
    T2CONbits.TMR2ON = TRUE;
}

void
vMBPortTimersDisable(  ) {
    /* Disable the timer. */
    T2CONbits.TMR2ON = FALSE;
#if MB_TIMER_ISR
    /* Disable the output compare interrupt */
    TMR2IE = FALSE;
#endif
    /* Clear output compare flags */
    TMR2IF = FALSE;
}

void vMBPortTimersDelay( USHORT usTimeOutMS ) {
    (void) usTimeOutMS;
}

void MB_Timer_ISR(void) {
    if (work.U8[1]) {
        --work.U8[1];
    } else if (work.U8[0]) {
        TMR2 = 0xff - work.U8[0];
        work.U8[0] = 0;
    } else {
       pxMBPortCBTimerExpired();
    }
}

