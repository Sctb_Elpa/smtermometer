/*
 * FreeModbus Libary: AVR Port
 * Copyright (C) 2006 Christian Walter <wolti@sil.at>
 *   - Initial version + ATmega168 support
 * Modfications Copyright (C) 2006 Tran Minh Hoang:
 *   - ATmega8, ATmega16, ATmega32 support
 *   - RS485 support for DS75176
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: $Id: port.h,v 1.6 2006/09/17 16:45:52 wolti Exp $
 */

#ifndef _PORT_H
#define _PORT_H

/* ----------------------- Platform includes --------------------------------*/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#endif

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#include "config.h"
#include "system.h"

/* ----------------------- Defines ------------------------------------------*/
#define	INLINE                      inline
#define PR_BEGIN_EXTERN_C           extern "C" {
#define	PR_END_EXTERN_C             }

#if 0
#define ENTER_CRITICAL_SECTION( )   do {INTCONbits.GIE = 0; } while(0)
#define EXIT_CRITICAL_SECTION( )    do {INTCONbits.GIE = 1; } while(0)
#else
#define ENTER_CRITICAL_SECTION( )
#define EXIT_CRITICAL_SECTION( )
#endif


#define MB_TIMER_ISR        0
#define ISR_SEAVE_CONTEXT   0

#define assert( x ) // disable assert

typedef bool    BOOL;

typedef uint8_t UCHAR;
typedef int8_t    CHAR;

typedef uint16_t USHORT;
typedef int16_t   SHORT;

typedef uint32_t ULONG;
typedef int32_t    LONG;

#ifndef TRUE
#define TRUE            true
#endif

#ifndef FALSE
#define FALSE           false
#endif

// TODO
/* ----------------------- RS485 specifics ----------------------------------*/
#ifdef  RTS_ENABLE

// using pull-up for io

#define RTS_PIN_DIR     TRISAbits.TRISA3
#define RTS_PIN         WPUAbits.WPUA3

#define RTS_INIT        \
    do { \
        RTS_PIN_DIR = 1; \
       /* RTS_PIN = 0; */ \
    } while( 0 );

#define RTS_HIGH        \
    do { \
        RTS_PIN = 1; \
    } while( 0 );

#define RTS_LOW         \
    do { \
        RTS_PIN = 0; \
    } while( 0 );

#endif

inline BOOL isTransmitting();

void SoftProcessMBEvents();
void MB_Timer_ISR(void);

#endif
