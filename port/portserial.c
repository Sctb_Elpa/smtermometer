/*
 * FreeModbus Libary: ATMega168 Port
 * Copyright (C) 2006 Christian Walter <wolti@sil.at>
 *   - Initial version and ATmega168 support
 * Modfications Copyright (C) 2006 Tran Minh Hoang:
 *   - ATmega8, ATmega16, ATmega32 support
 *   - RS485 support for DS75176
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: $Id: portserial.c,v 1.6 2006/09/17 16:45:53 wolti Exp $
 */

#include "port.h"

#include "system.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbport.h"

static BOOL tx_en;

void vMBPortSerialEnable( BOOL xRxEnable, BOOL xTxEnable )
{
    if( xTxEnable ) {
        RCSTAbits.CREN = xRxEnable;
#ifdef RTS_ENABLE
        RTS_HIGH;
#endif
        TXSTAbits.TXEN = TRUE;
    } else {
#ifdef RTS_ENABLE
        while(!TXSTAbits.TRMT);
        RTS_LOW;
        {
            uint8_t i = 0xff;
            while(i--);
        }
        RCSTAbits.CREN = xRxEnable;
#endif
        TXSTAbits.TXEN = FALSE;
    }
    tx_en = xTxEnable; 
}

inline BOOL isTransmitting() {
    return tx_en;
}

void vMBPortClose( void ) {
    vMBPortSerialEnable(FALSE, FALSE);
}

BOOL xMBPortSerialInit( UCHAR ucPORT, ULONG ulBaudRate, UCHAR ucDataBits,
        eMBParity eParity )
{
     /* prevent compiler warning. */
    (void)ucPORT;
    (void)ucDataBits;
    (void)eParity;
#if SYS_FREQ == 19200000ul
    SPBRGH=0x00;
    switch (ulBaudRate) {
        case 19200:
            SPBRGL=0x3d;
            break;
        case 38400:
            SPBRGL=0x1e;
            break;
        case 57600:
            SPBRGL=0x14;
            break;
        case 115200:
            SPBRGL=0x09;
            break;
        default:
            return FALSE;
    }
#elif SYS_FREQ == 10000000ul
    SPBRGH = 0x00;
    switch (ulBaudRate) {
        case 19200:
            SPBRGL = 32;
            break;
        case 38400:
            SPBRGL = 15;
            break;
        case 57600:
            SPBRGL = 10;
            break;
        case 115200:
            SPBRGL = 4;
            break;
        default:
            return FALSE;
    }
#else
#error "Boud not set for this SYS_FREQ, need recalc"
#endif

    vMBPortSerialEnable( FALSE, FALSE );
    
    TXSTAbits.BRGH = TRUE;  //Hi Speed
    TXSTAbits.TRMT = TRUE;
		
    RCSTAbits.SPEN = TRUE;
    
    BAUDCON=0x20; // ???

#ifdef RTS_ENABLE
    RTS_INIT;
#endif
    //TXSTAbits.TXEN = TRUE;

    return TRUE;
}

BOOL xMBPortSerialPutByte( CHAR ucByte ) {
    TXREG = ucByte;
    return TRUE;
}

BOOL xMBPortSerialGetByte( CHAR * pucByte ) {
    *pucByte = RCREG;
    return TRUE;
}
