/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#endif

#include <stdint.h>        /* For uint8_t definition */
#include <stdbool.h>       /* For true/false definition */
#include <string.h>

#include "Modbus.h"
#include "gloabal_vars.h"
#include "port.h"
#include "mb.h"

#define test_bit(REG, n)                ((REG) & (1 << (n)))
#define set_bit(REG, n)                 (REG) |= 1 << (n)
#define clear_bit(REG, n)               (REG) &= ~(1 << (n))

#define CoilsEnd                        0x02
#define DIEnd                           0x03
#define HREnd                           0x40
#define IREnd                           0x02

#define Passwordh                       0xab
#define Passwordl                       0x01
#define PassEnd                         0x05

#define WORK_HR_EEM_START               0

typedef uint16_t modbus_cell_t;

#define REG_INPUT_NREGS         (sizeof(double) / sizeof(modbus_cell_t))
#define DISCRET_INPUTS_NINPUTS  3
#define COILS_NCOILS            2
#define REG_HOLDING_NREGS       0x3A

static void __copy(uint8_t* to, uint8_t* from) { // swap bytes then copy
    *to++ = *(++from);
    *to = *(--from);
}

#if 0
static inline uint8_t rollleft1(uint8_t v) {
    return (v << 1) | 1;
}
#endif

eMBErrorCode
eMBRegInputCB( UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs )
{
    --usAddress;

    if (usAddress + usNRegs <= REG_INPUT_NREGS) {
        usAddress <<= 1;
        do {
            __copy(pucRegBuffer, &(T_value.RAW[usAddress]));
            usAddress += sizeof(modbus_cell_t);
            pucRegBuffer += sizeof(modbus_cell_t);
        } while (--usNRegs);
    } else {
        return MB_ENOREG;
    }

    return MB_ENOERR;
}

eMBErrorCode
eMBRegHoldingCB( UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs,
                 eMBRegisterMode eMode )
{
    USHORT Tmp;
    --usAddress;
        
    if (usAddress + usNRegs <= REG_HOLDING_NREGS) {
        if (eMode == MB_REG_READ) {
            // read
            usAddress <<= 1;
            do {
                eecpymem((uint8_t*)&Tmp, (__eeprom unsigned char *)
                            (WORK_HR_EEM_START + usAddress),
                        sizeof(modbus_cell_t));
                __copy(pucRegBuffer, (uint8_t*)&Tmp);

                pucRegBuffer += sizeof(modbus_cell_t);
                usAddress += sizeof(modbus_cell_t);
            } while (--usNRegs);
        } else {
            // write
            usAddress <<= 1;
            if (usAddress >= RW_HOLDINGS_START) {
               do {
                    __copy((uint8_t*)&Tmp, pucRegBuffer);
                    if (usAddress != 0x12)
                        memcpyee((__eeprom unsigned char *)
                            (WORK_HR_EEM_START + usAddress),
                            (uint8_t*)&Tmp, sizeof(modbus_cell_t));

                    pucRegBuffer += sizeof(modbus_cell_t);
                    usAddress += sizeof(modbus_cell_t);
                } while (--usNRegs);
            } else {
                return MB_ENORES;
            }
        }
    } else {
        return MB_ENOREG;
    }
    return MB_ENOERR; 
}


eMBErrorCode
eMBRegCoilsCB( UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNCoils,
               eMBRegisterMode eMode )
{
    --usAddress;

    if (usAddress + usNCoils <= COILS_NCOILS) {
        if (eMode == MB_REG_READ) {
            // read
            *pucRegBuffer = (coils.raw >> usAddress);
  #if 0
            for (tmp = ~(1 << (usNCoils + 1)); tmp != 0xff; tmp = rollleft1(tmp)) {
                *pucRegBuffer &= tmp;
            }
  #endif
        } else {
            // write
            // kostili for code size
            UCHAR tmp = *pucRegBuffer << ((uint8_t)usAddress);
            do {
                UCHAR v = (1 << usAddress);
                if (tmp & v) {
                    coils.raw |= v;
                } else {
                    coils.raw &= ~v;
                }
                ++usAddress;
            } while (--usNCoils);
        }
    } else {
        return MB_ENOREG;
    }

    return MB_ENOERR;
}

eMBErrorCode
eMBRegDiscreteCB( UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNDiscrete )
{
#if 0
    UCHAR tmp;
#endif
    --usAddress;

    if (usAddress + usNDiscrete <= DISCRET_INPUTS_NINPUTS) {
        *pucRegBuffer = DI.raw >> usAddress;
#if 0
        for (tmp = ~(1 << (usNDiscrete + 1)); tmp != 0xff; tmp = rollleft1(tmp)) {
            *pucRegBuffer &= tmp;
        }
#endif
    } else {
        return MB_ENOREG;
    }
    return MB_ENOERR;
}

