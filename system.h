/******************************************************************************/
/* System Level #define Macros                                                */
/******************************************************************************/

#ifndef _SYSTEM_H_
#define _SYSTEM_H_

#include "config.h"

/* Microcontroller MIPs (FCY) */
/* Define system operating frequency */
#ifdef F_CPU
#define SYS_FREQ                F_CPU
#define SYS_FREQ_PLL            (F_CPU * 4)
#else
#define DEFAULT_CPU_SPEED       19200000ul
#define SYS_FREQ                DEFAULT_CPU_SPEED
#define SYS_FREQ_PLL            (DEFAULT_CPU_SPEED * 4)
#endif

#if USE_PLL
#undef SYS_FREQ
#define SYS_FREQ                SYS_FREQ_PLL
#endif

// for __delay_ms()
#define _XTAL_FREQ              SYS_FREQ

/* Microcontroller MIPs (FCY) */
#define FCY                     (SYS_FREQ/4)

/******************************************************************************/
/* System Function Prototypes                                                 */
/******************************************************************************/

/* Custom oscillator configuration funtions, reset source evaluation
functions, and other non-peripheral microcontroller initialization functions
go here. */

inline void ConfigureOscillator(void); /* Handles clock switching/osc initialization */

// eeprom
#if 0
uint8_t EEPROM_read_wraper(uint8_t addr);
void EEPROM_write_wraper(uint8_t addr, uint8_t value);
#else
#define EEPROM_read_wraper(addr)        eeprom_read(addr)
#define EEPROM_write_wraper(addr, v)    eeprom_write(addr, v)
#endif

#endif /* _SYSTEM_H_ */