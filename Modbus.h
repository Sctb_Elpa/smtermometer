/* 
 * File:   Modbus.h
 * Author: tolyan
 *
 * Created on 29 ??????? 2016 ?., 13:42
 */

#ifndef MODBUS_H
#define	MODBUS_H

#ifdef	__cplusplus
extern "C" {
#endif

#define PASSWORD_H_ADDR             0x25
#define PASSWORD_L_ADDR             0x24

#define RW_HOLDINGS_START           0x02
#define WORK_HOLDINGS_START         0x20
#define RW_MAIN_HOLDINGS_START      0xb0

#define MB_ADDR_ADDR                0x20
#define MB_BAUD_ADDR                0x22

#define MEASURE_TIME_ADDR           0x2a

#define MEASURE_MINIMUM_TIME        100

#ifdef	__cplusplus
}
#endif

#endif	/* MODBUS_H */

