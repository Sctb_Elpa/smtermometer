/* 
 * File:   gloabal_vars.h
 * Author: tolyan
 *
 * Created on 24 ??????? 2016 ?., 13:40
 */

#ifndef GLOABAL_VARS_H
#define	GLOABAL_VARS_H

#ifdef	__cplusplus
extern "C" {
#endif

struct FlagsHolder {
    unsigned TmrOvf:1;					// overflow
    unsigned FError:1;					// device failure flag
    unsigned FErrCtr:1;					// failure control flag
    unsigned Ready:1;
};

union Coils {
    struct  {
        unsigned PowerSaveMode:1;
        unsigned WriteSettings:1;
    } _Coils;
    uint8_t raw;
};

union DInputs {
    struct  {
        unsigned Failure:1;
        unsigned T_Crytical_sensor:1;
        unsigned Invalid_settings:1;
    } _DInputs;
    uint8_t raw;
};

union Double2Bytes
{
    double doubleVal;
    uint8_t RAW[4];
};

union U16_2U8 {
    uint16_t    U16;
    uint8_t     U8[2];
};

union u32_16_8
{
    uint32_t    U32;
    uint16_t    U16[2];
    uint8_t     U8[4];
};

extern struct FlagsHolder flagsHolder;
extern union Coils coils;
extern union DInputs DI;
extern union Double2Bytes T_value;
extern union U16_2U8 StartTimeNew;
extern union U16_2U8 StartTimeWas;
extern uint32_t TimeNew;
extern uint32_t TimeOld;

#ifdef	__cplusplus
}
#endif

#endif	/* GLOABAL_VARS_H */

