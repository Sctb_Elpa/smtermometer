/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#endif

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>

#include "port/port.h"
#include "gloabal_vars.h"


/******************************************************************************/
/* Interrupt Routines                                                         */
/******************************************************************************/

/* Baseline devices don't have interrupts. Unfortunately the baseline detection 
 * macro is named _PIC12 */

static uint8_t ErrTimer;
static uint8_t TMR0_Extender = 0xff;
static uint16_t T1_ext;

#ifndef _PIC12

void interrupt isr(void) {
#if ISR_SEAVE_CONTEXT
    uint8_t Status_temp = STATUS;
    uint8_t BSR_tmp = BSR;
#endif
    union u32_16_8 longCounter;

    if (TMR0IF) {                               // is TMR0 interrupt ?
        TMR0IF = false;                         // reset interrupt flag

        //capture counter
        TMR1ON = FALSE;
        longCounter.U16[0] = TMR1;
        longCounter.U16[1] = T1_ext;
        if(PIR1bits.TMR1IF)
            ++longCounter.U16[1];
        TMR1ON = TRUE;

        ++TMR0_Extender;
        if (!TMR0_Extender) {
            TMR0_Extender = StartTimeNew.U8[1]; // restart counter and extander
            TMR0 += StartTimeNew.U8[0];
            TimeOld = TimeNew;
            TimeNew = longCounter.U32;
            flagsHolder.Ready = true;
        }
    }
    
    if (TMR1IF)	{				// is TMR1 interrupt ?
        TMR1IF = false;    			// reset interrupt flag
        ++ErrTimer;
        if (ErrTimer == 0xdb) {
            ErrTimer = 0;
            DI._DInputs.Failure = flagsHolder.FErrCtr;
            flagsHolder.FErrCtr = true;
        }
        ++T1_ext;
    }
       
#if MB_TIMER_ISR
    if (TMR2IF) {
        TMR2IF = false;
        MB_Timer_ISR();
    }
#endif

#if ISR_SEAVE_CONTEXT
    BSR = BSR_tmp;
    STATUS = Status_temp;
#endif
}


#endif


