/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#endif

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */
#include <math.h>

#include "mb.h"

#include "Modbus.h"
#include "gloabal_vars.h"
#include "system.h"
#include "user.h"


#define DEFAULT_FREQ            30000
#define DEFAULT_MEASURE_TIME    1000

#define FCY_MUL_CROP            1000
#define FCY_CROPP               (FCY / FCY_MUL_CROP)

static struct T_Coeffs coeffs;
static uint16_t measure_period;
static double prevF;

/******************************************************************************/
/* User Functions                                                             */
/******************************************************************************/

static inline void disable_peripherial() {
    FVRCON=0x00;
    ADCON0=0x00;				//ADC Off
    ADCON1=0x00;
    
    DACCON0=0x00;				//DAC Off
    DACCON1=0x00;
    
    SRCON0=0x00;				//SR Off
    SRCON1=0x00;
    
    CM1CON0=0x00;				//Comparator Off
    CM1CON1=0x00;
    
    MDCON=0x00;                                 // DSM Off
    MDSRC=0x80;
    MDCARH=0x80;
    MDCARL=0x80;
    
    //CCP1CON=0x00;                               // CCP off
    
    //SSP1CON1=0x00;                              // i2c off
    
    //CPSCON0=0x00;                               // Capacitive Sensing
    //CPSCON1=0x00;
}

static inline void init_interrupts() {
    PIE1 = 0x01;				// interrupt UART, timer 1,2 0x23 0x01
    PIE2 = 0x00;
    PIR1 = 0x00;				// reset interrupt flags
    PIR2 = 0x00;
    IOCAP = 0x00;                               // interrupt on change pin state
    IOCAN = 0x00;                               // disable
    IOCAF = 0x00;
    INTCON = 0xe0;
}

static inline void setup_iopins() {
    PORTA=0x00;
    LATA=0x00;
    ANSELA=0x00;				//digital I/O
    TRISA=0x3e;					//RA0 is input
    APFCON0=0x00;
    //WPUA=0x00;				//pull up off
}

static inline void init_peripherals() {
    OPTION_REG=0x28;                            //Timer0 in RA2
    T1CON=0x01;					//Timer1 On
    T1GCON=0x00;
    T2CON=0x6a;					//Timer2 Off
    PR2=0xff;
}

static void EEwrite_if_unequal(uint8_t addr, uint8_t expect_val) {
    if (EEPROM_read_wraper(addr) != expect_val)
        EEPROM_write_wraper(addr, expect_val);
}

static void reload_measure_period(double F) {
    TMR0IE = FALSE; // prevernt interrupt and update this values asyncroniusly
    StartTimeWas = StartTimeNew;
    StartTimeNew.U16 = ~((uint16_t)(measure_period * F / 1000));
    if (StartTimeNew.U16 == 0xffff) {
        DI._DInputs.Invalid_settings = true;
        StartTimeNew.U8[0] = 0xfe;
    }
    TMR0IE = TRUE;
}

// size optimisation
static void load_default_period() {
    reload_measure_period(DEFAULT_FREQ);
}

static void Load(void)
{
    eecpymem((uint8_t*)&coeffs,
        (__eeprom unsigned char *)0x60, sizeof(struct T_Coeffs));
    eecpymem((uint8_t*)&measure_period,
        (__eeprom unsigned char *)MEASURE_TIME_ADDR, sizeof(uint16_t));
    if (measure_period < MEASURE_MINIMUM_TIME) {
        measure_period = MEASURE_MINIMUM_TIME;
        DI._DInputs.Invalid_settings = true;
    }
    coils.raw = EEPROM_read_wraper(0x81);

    load_default_period();
}

static void EECopySegment(uint8_t to, uint8_t from, uint8_t size) {
    do {
        EEwrite_if_unequal(to++, EEPROM_read_wraper(from++));
    } while (--size);   
}

static inline void WrNewData(void) {
    // MBAdr + Baud
    EECopySegment(0xb0, 0x20, 4);
}

static void createModbus() {
    uint16_t Tmp;
    eMBErrorCode err;
    eecpymem((uint8_t*)&Tmp,
        (__eeprom unsigned char *)MB_BAUD_ADDR, sizeof(uint16_t));
    if (Tmp == 0xffff) {
        err = eMBInit(MB_RTU, EEPROM_read_wraper(MB_ADDR_ADDR),
                0, 115200, MB_PAR_NONE);
    } else {
        err = eMBInit(MB_RTU, EEPROM_read_wraper(MB_ADDR_ADDR),
                0, Tmp, MB_PAR_NONE);
    }

    if (err != MB_ENOERR) {
        eMBInit(MB_RTU, 1, 0, 57600, MB_PAR_NONE);
        DI._DInputs.Invalid_settings = TRUE;
    }
    eMBEnable();
}

void InitApp(void) {
    /* disable unused peripherals */
    disable_peripherial();
    
    /* Setup analog functionality and port direction */
    setup_iopins();

    /* Init peripherals */
    init_peripherals();

    createModbus();

    // flags
    //flagsHolder.Ready = 0;
    //flagsHolder.TmrOvf = 0;

    /* Setup interrupts */
    init_interrupts();
}

void PrepareData(void) {
    //reset password
    EEwrite_if_unequal(PASSWORD_L_ADDR, 0);
    EEwrite_if_unequal(PASSWORD_H_ADDR, 0);

    // MBAdr + Baud
    EECopySegment(WORK_HOLDINGS_START, RW_MAIN_HOLDINGS_START, 4);

    Load();

    load_default_period();

    coils._Coils.WriteSettings = false;

    eMBEnable();
}

static void process_coils() {
    if (coils._Coils.WriteSettings && !isTransmitting()) {
        coils._Coils.WriteSettings = false;
        EEwrite_if_unequal(0x81, coils.raw);

        WrNewData();
        Load();
        
        //eMBClose();
        createModbus();
    }
}

static void hiPrioProcess() {
    SoftProcessMBEvents();
    eMBPoll();
#ifndef __DEBUG
    CLRWDT();
#endif
}

void main_loop() {
    if ((flagsHolder.Ready) && (!isTransmitting()))
    {
        double Fin;
        uint8_t i;
        uint16_t st_time;
        
        union {
            double F_pow_tmp;
            uint32_t period;
            int32_t s_period;
        } Tmp;

        TMR0IE = FALSE; // prevernt interrupt and update this values asyncroniusly
        Tmp.period = TimeNew - TimeOld;
        st_time = StartTimeWas.U16;
        TMR0IE = TRUE;
        if (Tmp.period & (1ul << (sizeof(Tmp) * 8 - 1))) {
            Tmp.period = (1ul << (sizeof(Tmp) * 8 - 1)) - TimeOld + TimeNew;
        }

        Fin = (double)((~st_time) + 1) / (double)Tmp.period * (double)FCY;
        if (DI._DInputs.Failure = ((Fin < Fmin) || (Fin > Fmax))) {
            load_default_period();
        } else {
            reload_measure_period(Fin);

            hiPrioProcess();

            Fin -= coeffs.F0;
            T_value.doubleVal = 0.0;
            for (i = 0, Tmp.F_pow_tmp = Fin; i < 3; ++i, Tmp.F_pow_tmp *= Fin) {
                T_value.doubleVal += coeffs.C[i] * Tmp.F_pow_tmp;
                hiPrioProcess();
            }
            T_value.doubleVal += coeffs.T0;

            flagsHolder.FErrCtr = false;

            DI._DInputs.T_Crytical_sensor =
                ((T_value.doubleVal > Tmax) || (T_value.doubleVal < Tmin));
        }

        flagsHolder.Ready = false;
    }

    hiPrioProcess();
    process_coils();
}
