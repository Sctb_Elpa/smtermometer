/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#endif

#include <stdint.h>        /* For uint8_t definition */
#include <stdbool.h>       /* For true/false definition */

#include "gloabal_vars.h"

struct FlagsHolder flagsHolder;
union Coils coils;
union DInputs DI;
union Double2Bytes T_value;
union U16_2U8 StartTimeNew;
union U16_2U8 StartTimeWas;
uint32_t TimeNew;
uint32_t TimeOld;