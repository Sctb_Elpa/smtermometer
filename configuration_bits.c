/******************************************************************************/
/* Main Files to Include                                                      */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#endif

#include "config.h"

/******************************************************************************/
/* Configuration Bits                                                         */
/*                                                                            */
/* Refer to your Hi-Tech User Manual in the PICC installation directory       */
/* /doc folder for more information on filling in configuration bits.         */
/* In addition, configuration bit mnemonics can be found in your              */
/* PICC\version\include\<processor name>.h file for your device.  The XC8     */
/* compiler contains documentation on the configuration bit macros within     */
/* the compiler installation /docs folder in a file called                    */
/* pic18_chipinfo.html.                                                       */
/*                                                                            */
/* For additional information about what the hardware configurations mean in  */
/* terms of device operation, refer to the device datasheet.                  */
/*                                                                            */
/* A feature of MPLAB X is the 'Generate Source Code to Output' utility in    */
/* the Configuration Bits window.  Under Window > PIC Memory Views >          */
/* Configuration Bits, a user controllable configuration bits window is       */
/* available to Generate Configuration Bits source code which the user can    */
/* paste into this project.                                                   */
/*                                                                            */
/******************************************************************************/

// common
#pragma config FOSC = HS, PWRTE = ON, MCLRE = OFF
#pragma config BOREN = NSLEEP, CLKOUTEN = OFF, IESO = OFF, FCMEN = OFF
#pragma config STVREN = ON, BORV = HI, LVP = OFF
#if USE_PLL
#   pragma config PLLEN = ON
#else
#   pragma config PLLEN = OFF
#endif

#ifdef __DEBUG
#   warning "Debug build!"
#   pragma config CP = ON, CPD = ON
#   pragma config WRT = ALL
#   pragma config WDTE = OFF
#else
#   pragma config CP = OFF, CPD = OFF
#   pragma config WRT = OFF
#   pragma config WDTE = ON
#endif
