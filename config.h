/* 
 * File:   config.h
 * Author: tolyan
 *
 * Created on 9 ??????? 2016 ?., 13:28
 */

#ifndef CONFIG_H
#define	CONFIG_H

#ifdef	__cplusplus
extern "C" {
#endif

#define OK              0
#define ERROR           1

typedef char error_t;


#define USE_PLL                 0

#define IO_PIN_INPUT            (TRUE)
#define IO_PIN_OUTPUT           (FALSE)

#define Fcenter                 32700.0
#define F_diff_abs              400.0
#define F_diff_abs_p            (F_diff_abs / 2)
#define Fmin                    (Fcenter - F_diff_abs)
#define Fmax                    (Fcenter + F_diff_abs)
#define Tmin                    (double)-45.0 
#define Tmax                    (double)125.0

#define RTS_ENABLE

#define MB_SER_PDU_SIZE_MAX     24

#ifdef	__cplusplus
}
#endif

#endif	/* CONFIG_H */

