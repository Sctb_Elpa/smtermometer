/******************************************************************************/
/* User Level #define Macros                                                  */
/******************************************************************************/

struct T_Coeffs {
    double T0;
    double C[3];
    double F0;
};


/******************************************************************************/
/* User Function Prototypes                                                   */
/******************************************************************************/

void InitApp(void);         /* I/O and Peripheral Initialization */
void PrepareData(void);     /* Prepare date tables after reset */
void main_loop(void);       /* User application */
void PrepareData(void);
